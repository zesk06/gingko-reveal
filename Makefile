.PHONY: all clean serve

T_DIR := target/site

LICENCE := ${T_DIR}/LICENCE
INDEX := ${T_DIR}/index.html

SRC := src/gingko.json

TITLEFLAG=${GINGKOTITLE}# if env variable TITLE is present it is used to inject presentation title
ifneq '${TITLEFLAG}' ''
TITLEFLAG := -t "$(TITLEFLAG)"
endif


REVEAL_VERSION := 3.8.0
REVEAL_TGZ=vendors/reveal.js-${REVEAL_VERSION}.tar.gz

# Following lines permits to go Makefile debug
# OLD_SHELL := $(SHELL)
# SHELL = $(warning [$@ ($^) ($?)])$(OLD_SHELL)

all: ${INDEX}

dl:
ifdef GINGKOTREE
	curl -u $(GINGKOCRED) https://gingkoapp.com/api/export/$(GINGKOTREE).json > ${SRC}
else
	@echo "Please set GINGKOTREE and GINGKOAPP env var"
	@echo "#export GINGKOTREE=5f6bfbe95d73b68e95000027"
	@echo "#export GINGKOCRED=user@here.org:passwd sans :passwd pour obtenir un prompt"

endif

install:
	pip3 install -r requirements.txt
	pre-commit install

installpathpy:
	@if test "$(shell pip3 list --format=columns | grep path.py | wc -l)" = "0" ; then pip3 install path.py ; else echo "Nothing to do" ; fi

clean:
	@echo "clean"
	@rm -fr target	
	@rm -fr vendors

serve:
	@ag -l | entr make dl&
	@cd target/site && python3 -m http.server 9000&

html: 
	@python3 src/gingkoc.py ${TITLEFLAG} -o ${INDEX} ${SRC}

flathtml: 
	@python3 src/gingkoc.py --flatten ${TITLEFLAG} -o ${INDEX} ${SRC}

dlhtml: dl html

${REVEAL_TGZ}:
	@echo "Download ${REVEAL_TGZ}"
	@mkdir -p vendors
	@wget -q -O ${REVEAL_TGZ} https://codeload.github.com/hakimel/reveal.js/tar.gz/3.8.0
	@rm -fr target/site

# On se sert de la licence pour détecter qu'on a bien dépackagé
${LICENCE}: ${REVEAL_TGZ}
	@echo "unpack ${REVEAL_TGZ}"
	@rm -fr target/site
	@mkdir -p target
	@tar -C target -xf vendors/reveal.js-3.8.0.tar.gz
	@mv target/reveal.js-3.8.0 ${T_DIR}
	@rm target/site/index.html
	@touch ${LICENCE}
	@echo "reveal deployed"
	@cp -r templates/static/* target/site/
	@echo "static template injected"
	@rm -rf target/site/test
	@rm target/site/package-lock.json target/site/demo.html target/site/bower.json target/site/README.md  target/site/package.json target/site/gruntfile.js  target/site/CONTRIBUTING.md 
	@echo "unused file cleaned up"
	@echo "site initialized in ${T_DIR}"


${INDEX}: ${LICENCE} templates/index.html ${SRC}
	@echo "generating using src/gingkoc.py -i ./src/img -r ./img -o ${INDEX} ${SRC}"
	@rm -rf src/img
	@rm -rf target/site/img
	@mkdir -p src/img
	@python3 src/gingkoc.py ${TITLEFLAG} -p -i ./src/img -r ./img -o ${INDEX} ${SRC}
	@cp -r  src/img target/site/


