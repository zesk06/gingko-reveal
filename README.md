# gingko-reveal

Ce projet permet de transformer un export json depuis 
[gingko-app](https://gingkoapp.com) to a [reveal](https://revealjs.com/#/) 
presentation.

il permet de rapidement prototyper une présentation avec gingko, 
de la transformer en 
reveal avant de la reprendre en html.

## comment qu'on installe

```bash
# Install d'un python3x, pip et curl si besoin
sudo apt install python3.7
sudo apt install python3-pip
sudo apt install curl
# pour le live reload...
sudo apt install entr
sudo apt install silversearcher-ag

# Install des dépendances python
make install
```

## comment qu'on fait

- Export de l'arbre gingko en json dans ``src/gingko.json``. 
Manuellement ou avec ``make dl`` en ayant les variables d'environnement de 
positionnées ``export GINGKOCRED=moi@chezmoi.org`` et ``export GINGKOTREE=123456879``
- on appelle ``make``, le site est généré dans target/site

## comment qu'on developpe

- on passe en mode ``live`` avec ``make serve``
- on indente avec ``black``

## comment qu'on voit le résultat

- on va sur la page [publiée](https://zesk06.frama.io/gingko-reveal/)

## TODO

- faire de belles présentations...
