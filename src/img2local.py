#!/usr/bin/env python3
# encoding: utf-8

"""Ce script télécharge les images en local"""

import os
import re
import sys
import urllib.parse
import urllib.request
from base64 import b64decode


class Salt:
    class __Singleton:
        def __init__(self, arg):
            self.val = arg

        def __str__(self):
            return repr(self) + self.val

    instance = None

    def __init__(self, salt=0):
        if not Salt.instance:
            Salt.instance = Salt.__Singleton(salt)
        else:
            Salt.instance.val = salt

    def __str__(self):
        ret = str(self.instance.val) + "-"
        self.instance.val += 1
        return ret


class ImgResolver:
    def __init__(
        self,
        agent=("User-agent", "Mozilla/5.0"),
        destpath="./img/",
        replacepath="./img/",
        suffix="img",
    ):
        opener = urllib.request.build_opener()
        opener.addheaders = [agent]
        urllib.request.install_opener(opener)
        self.__salt = Salt()
        self.__path = os.path.join(destpath, "")
        self.__replacepath = os.path.join(replacepath, "")
        self.__suffix = suffix

    def datauriserial(self, reg):
        data_uri = reg.group(2)
        header, encoded = data_uri.split(",", 1)
        data = b64decode(encoded)
        # print(header)
        ext = (
            re.search(".*image/(.+);.*", header).group(1)
            if re.search(".*image/(.+);.*", header) is not None
            else ""
        )
        salt = str(self.__salt)
        local_filename = self.__path + salt + self.__suffix + "." + ext
        replace_local_filename = self.__replacepath + salt + self.__suffix + "." + ext
        with open(local_filename, "wb") as f:
            f.write(data)
        return (
            "!["
            + reg.group(1)
            + "]("
            + replace_local_filename
            + " "
            + (reg.group(3) if reg.group(3) is not None else "")
            + ")"
        )

    def dl(self, reg):
        url = reg.group(2)
        if url.startswith("data:"):
            return self.datauriserial(reg)

        split = urllib.parse.urlsplit(url)
        salt = str(self.__salt)
        local_filename = self.__path + salt + split.path.split("/")[-1]
        replace_local_filename = self.__replacepath + salt + split.path.split("/")[-1]
        print('=> "' + replace_local_filename + '" local"' + local_filename + '"')

        local_filename, headers = urllib.request.urlretrieve(url, local_filename)
        return (
            "!["
            + reg.group(1)
            + "]("
            + replace_local_filename
            + " "
            + (reg.group(3) if reg.group(3) is not None else "")
            + ")"
        )

    def process(self, line):
        reg = '!\[([^\]]*)\]\(([^" ]*) *(".*")?\)'
        return (
            re.sub(reg, self.dl, line) + "\n"
            if re.search(reg, line) is not None
            else line
        )


# reg='!\[([^\]]*)\]\(([^"]*) ?(".*")?\)';
# [sys.stdout.write(re.sub(reg,dl,line) +'\n' if re.search(reg, line) is not None else line) for line in sys.stdin];
#
# python3 -c "import sys,re;reg='\(da.?a';[sys.stdout.write(re.search(reg,line).group() +'\n' if re.search(reg, line) is not None else '') for line in sys.stdin];" < gingko.json
