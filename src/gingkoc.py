#!/usr/bin/env python3
# encoding: utf-8
import argparse
import json
import logging

from img2local import ImgResolver  # local imager resolver helper
from jinja2 import Environment, FileSystemLoader, Template
from path import Path

THIS_DIR = Path(__file__).abspath().dirname()
TEMPLATE_DIR = THIS_DIR / ".." / "templates"
logging.basicConfig(level=logging.INFO)

LOGGER = logging.getLogger("GING")


PARSER = argparse.ArgumentParser("gingkoc")
PARSER.add_argument("--image-path", "-i", help="the path where to import images")
PARSER.add_argument(
    "--relative-path", "-r", help="the relative path to use images from"
)
PARSER.add_argument("--title", "-t", help="the html file <title>")
PARSER.add_argument(
    "--flatten", "-f", help="output is horizontally layouted", action="store_true"
)
PARSER.add_argument("--output-file", "-o", help="the output file")
PARSER.add_argument("--production", "-p", help="No livejs", action="store_true")
PARSER.add_argument("FILE", help="The gingko json input files")


def j2_template():
    """return the template"""
    j2_env = Environment(loader=FileSystemLoader(TEMPLATE_DIR))
    return j2_env.get_template("index.html")


class Card:
    def __init__(self, content="", level=0):
        self.__content = content
        self.level = level

    def __repr__(self):
        summary = self.content.replace("\n", "")[:10]
        return f"Card({summary}, {self.level}"

    @staticmethod
    def from_json(json_data, level=0):
        cards = []

        if "content" in json_data:
            level_card = Card(json_data["content"], level=level)
            if json_data["content"]:
                cards.append(level_card)
        if "children" in json_data:
            for child_json in json_data["children"]:
                cards.extend(Card.from_json(child_json, level=level + 1))
        return cards

    @property
    def content(self):
        return self.__content

    @content.setter
    def content(self, value):
        self.__content = value


def load_json(json_file=THIS_DIR / "gingko.json"):
    """Load gingko.json"""
    LOGGER.info(f"loading json from {json_file}")
    json_data = json.loads(json_file.text())
    return json_data


def json_to_cards(json_data):
    carddeck = []
    for level0_card in json_data:
        new_cards = Card.from_json(level0_card)
        if len(new_cards) > 0:
            carddeck.append(new_cards)
    return carddeck


def main(args):

    title = args.title
    flatten = args.flatten
    input_file = Path(args.FILE)
    output_file = Path(args.output_file)
    local_image = None if not args.image_path else Path(args.image_path)
    relative_path = None if not args.relative_path else Path(args.relative_path)
    production = args.production
    # load cards
    carddeck = json_to_cards(load_json(json_file=input_file))
    # if required localize images
    if local_image:
        assert local_image.exists(), "image_path does not exist"
        assert local_image.isdir(), "image_path is not a directory"
        LOGGER.info(f"Resolving images to local files")
        ir = ImgResolver(destpath=local_image, replacepath=relative_path)
        for cards in carddeck:
            for card in cards:
                setattr(card, "content", ir.process(card.content))
    if flatten:
        flat_list = [[item] for sublist in carddeck for item in sublist]
        carddeck = flat_list

    # load template
    TEMPLATE = j2_template()
    html_text = TEMPLATE.render(carddeck=carddeck, production=production, title=title)
    if output_file:
        LOGGER.info(f"writing { output_file }")
        output_file.write_text(html_text)
    else:
        print(html_text)


if __name__ == "__main__":
    args = PARSER.parse_args()
    main(args)
